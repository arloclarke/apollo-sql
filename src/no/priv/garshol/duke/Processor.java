package no.priv.garshol.duke;

import java.io.PrintWriter;
import java.io.Writer;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

import mimir.ProgressTracker;
import no.priv.garshol.duke.Configuration;
import no.priv.garshol.duke.DataSource;
import no.priv.garshol.duke.Database;
import no.priv.garshol.duke.Duke;
import no.priv.garshol.duke.DukeException;
import no.priv.garshol.duke.DummyLogger;
import no.priv.garshol.duke.Logger;
import no.priv.garshol.duke.Property;
import no.priv.garshol.duke.Record;
import no.priv.garshol.duke.RecordIterator;
import no.priv.garshol.duke.matchers.AbstractMatchListener;
import no.priv.garshol.duke.matchers.MatchListener;
import no.priv.garshol.duke.matchers.PrintMatchListener;
import no.priv.garshol.duke.utils.Utils;

import no.priv.garshol.duke.ConfigLoader;

public class Processor {
    private Configuration config;
    protected Database database;
    private Collection<MatchListener> listeners;
    private Logger logger;
    private List<Property> proporder;
    private double[] accprob;
    private int threads;
    private static final int DEFAULT_BATCH_SIZE = 40000;
    private long comparisons;
    private long srcread;
    private long indexing;
    private long searching;
    private long comparing;
    private long callbacks;
    private Processor.Profiler profiler;

    public Processor(Configuration config) {
        this(config, true);
    }

    public Processor(Configuration config, boolean overwrite) {
        this(config, config.getDatabase(overwrite));
    }

    public Processor(Configuration config, Database database) {
        this.config = config;
        this.database = database;
        this.listeners = new CopyOnWriteArrayList();
        this.logger = new DummyLogger();
        this.threads = 1;
        this.proporder = new ArrayList();
        Iterator prob = config.getProperties().iterator();

        while(prob.hasNext()) {
            Property p = (Property)prob.next();
            if(!p.isIdProperty()) {
                this.proporder.add(p);
            }
        }

        Collections.sort(this.proporder, new Processor.PropertyComparator());
        double var6 = 0.5D;
        this.accprob = new double[this.proporder.size()];

        for(int ix = this.proporder.size() - 1; ix >= 0; --ix) {
            var6 = Utils.computeBayes(var6, ((Property)this.proporder.get(ix)).getHighProbability());
            this.accprob[ix] = var6;
        }

    }

    public void setLogger(Logger logger) {
        this.logger = logger;
    }

    public void setThreads(int threads) {
        this.threads = threads;
    }

    public int getThreads() {
        return this.threads;
    }

    public void addMatchListener(MatchListener listener) {
        this.listeners.add(listener);
    }

    public boolean removeMatchListener(MatchListener listener) {
        return listener != null?this.listeners.remove(listener):true;
    }

    public Collection<MatchListener> getListeners() {
        return this.listeners;
    }

    public Database getDatabase() {
        return this.database;
    }

    public void setPerformanceProfiling(boolean profile) {
        if(profile) {
            if(this.profiler != null) {
                return;
            }

            this.profiler = new Processor.Profiler();
            this.addMatchListener(this.profiler);
        } else {
            if(this.profiler == null) {
                return;
            }

            this.removeMatchListener(this.profiler);
            this.profiler = null;
        }

    }

    public Processor.Profiler getProfiler() {
        return this.profiler;
    }

    public void deduplicate() {
        this.deduplicate(this.config.getDataSources(), '鱀');
    }

    public void deduplicate(int batch_size) {
        this.deduplicate(this.config.getDataSources(), batch_size);
    }

    public void deduplicate(Collection<DataSource> sources, int batch_size) {
        int count = 0;
        this.startProcessing();
        Iterator it = sources.iterator();

        while(it.hasNext()) {
            DataSource source = (DataSource)it.next();
            source.setLogger(this.logger);
            RecordIterator it2 = source.getRecords(source);

            try {
                ArrayList batch = new ArrayList();
                long start = System.currentTimeMillis();

                while(it2.hasNext()) {
                    ProgressTracker.addCount();
                    ProgressTracker.getPercent();
                    Record record = (Record)it2.next();
                    batch.add(record);
                    ++count;
                    if(count % batch_size == 0) {
                        this.srcread += System.currentTimeMillis() - start;
                        this.deduplicate(batch);
                        it2.batchProcessed();
                        batch = new ArrayList();
                        start = System.currentTimeMillis();
                    }
                }
                if(!batch.isEmpty()) {
                    this.deduplicate(batch);
                    it2.batchProcessed();
                }
            } finally {
                it2.close();
            }
        }
        this.endProcessing();
    }

    public void deduplicate(Collection<Record> records) {
        this.logger.info("Deduplicating batch of " + records.size() + " records");
        this.batchReady(records.size());
        long start = System.currentTimeMillis();
        Iterator i$ = records.iterator();

        while(i$.hasNext()) {
            Record record = (Record)i$.next();
            this.database.index(record);
        }

        this.database.commit();
        this.indexing += System.currentTimeMillis() - start;
        this.match(records, true);
        this.batchDone();
    }

    private void match(Collection<Record> records, boolean matchall) {
        if(this.threads == 1) {
            Iterator i$ = records.iterator();

            while(i$.hasNext()) {
                Record record = (Record)i$.next();
                this.match(record, matchall);
            }
        } else {
            this.threadedmatch(records, matchall);
        }

    }

    private void threadedmatch(Collection<Record> records, boolean matchall) {
        Processor.MatchThread[] threads = new Processor.MatchThread[this.threads];

        int ix;
        for(ix = 0; ix < threads.length; ++ix) {
            threads[ix] = new Processor.MatchThread(ix, records.size() / threads.length, matchall);
        }

        ix = 0;
        Iterator e = records.iterator();

        while(e.hasNext()) {
            Record record = (Record)e.next();
            threads[ix++ % threads.length].addRecord(record);
        }

        for(ix = 0; ix < threads.length; ++ix) {
            threads[ix].start();
        }

        try {
            for(ix = 0; ix < threads.length; ++ix) {
                threads[ix].join();
            }
        } catch (InterruptedException var7) {
            ;
        }

    }

    public void link() {
        this.link(this.config.getDataSources(1), this.config.getDataSources(2), '鱀');
    }

    public void link(Collection<DataSource> sources1, Collection<DataSource> sources2, int batch_size) {
        this.link(sources1, sources2, true, batch_size);
    }

    public void link(Collection<DataSource> sources1, Collection<DataSource> sources2, boolean matchall, int batch_size) {
        this.startProcessing();
        this.index(sources1, batch_size);
        this.linkRecords(sources2, matchall, batch_size);
    }

    public void linkRecords(Collection<DataSource> sources) {
        this.linkRecords(sources, true);
    }

    public void linkRecords(Collection<DataSource> sources, boolean matchall) {
        this.linkRecords(sources, matchall, '鱀');
    }

    public void linkRecords(Collection<DataSource> sources, boolean matchall, int batch_size) {
        Iterator i$ = sources.iterator();

        while(i$.hasNext()) {
            DataSource source = (DataSource)i$.next();
            source.setLogger(this.logger);
            ArrayList batch = new ArrayList(batch_size);
            RecordIterator it = source.getRecords(source);

            while(it.hasNext()) {
                batch.add(it.next());
                if(batch.size() == batch_size) {
                    this.linkBatch(batch, matchall);
                    batch.clear();
                }
            }

            it.close();
            if(!batch.isEmpty()) {
                this.linkBatch(batch, matchall);
            }
        }

        this.endProcessing();
    }

    private void linkBatch(Collection<Record> batch, boolean matchall) {
        this.batchReady(batch.size());
        this.match(batch, matchall);
        this.batchDone();
    }

    public void index(Collection<DataSource> sources, int batch_size) {
        int count = 0;
        Iterator i$ = sources.iterator();

        while(i$.hasNext()) {
            DataSource source = (DataSource)i$.next();
            source.setLogger(this.logger);
            RecordIterator it2 = source.getRecords(source);

            while(it2.hasNext()) {
                Record record = (Record)it2.next();
                this.database.index(record);
                ++count;
                if(count % batch_size == 0) {
                    this.batchReady(batch_size);
                }
            }

            it2.close();
        }

        if(count % batch_size == 0) {
            this.batchReady(count % batch_size);
        }

        this.database.commit();
    }

    public long getComparisonCount() {
        return this.comparisons;
    }

    private void match(Record record, boolean matchall) {
        long start = System.currentTimeMillis();
        Collection candidates = this.database.findCandidateMatches(record);
        this.searching += System.currentTimeMillis() - start;
        if(this.logger.isDebugEnabled()) {
            this.logger.debug("Matching record " + PrintMatchListener.toString(record, this.config.getProperties()) + " found " + candidates.size() + " candidates");
        }

        start = System.currentTimeMillis();
        if(matchall) {
            this.compareCandidatesSimple(record, candidates);
        } else {
            this.compareCandidatesBest(record, candidates);
        }

        this.comparing += System.currentTimeMillis() - start;
    }

    protected void compareCandidatesSimple(Record record, Collection<Record> candidates) {
        boolean found = false;
        Iterator i$ = candidates.iterator();

        while(i$.hasNext()) {
            Record candidate = (Record)i$.next();
            if(!this.isSameAs(record, candidate)) {
                double prob = this.compare(record, candidate);
                if(prob > this.config.getThreshold()) {
                    found = true;
                    this.registerMatch(record, candidate, prob);
                } else if(this.config.getMaybeThreshold() != 0.0D && prob > this.config.getMaybeThreshold()) {
                    found = true;
                    this.registerMatchPerhaps(record, candidate, prob);
                }
            }
        }

        if(!found) {
            this.registerNoMatchFor(record);
        }

    }

    protected void compareCandidatesBest(Record record, Collection<Record> candidates) {
        double max = 0.0D;
        Record best = null;
        Iterator i$ = candidates.iterator();

        while(i$.hasNext()) {
            Record candidate = (Record)i$.next();
            if(!this.isSameAs(record, candidate)) {
                double prob = this.compare(record, candidate);
                if(prob > max) {
                    max = prob;
                    best = candidate;
                }
            }
        }

        if(max > this.config.getThreshold()) {
            this.registerMatch(record, best, max);
        } else if(this.config.getMaybeThreshold() != 0.0D && max > this.config.getMaybeThreshold()) {
            this.registerMatchPerhaps(record, best, max);
        } else {
            this.registerNoMatchFor(record);
        }

    }

    public double compare(Record r1, Record r2) {
        ++this.comparisons;
        double prob = 0.5D;
        Iterator i$ = r1.getProperties().iterator();

        label80:
        while(true) {
            Property prop;
            Collection vs1;
            Collection vs2;
            do {
                do {
                    do {
                        do {
                            String propname;
                            do {
                                do {
                                    do {
                                        if(!i$.hasNext()) {
                                            return prob;
                                        }

                                        propname = (String)i$.next();
                                        prop = this.config.getPropertyByName(propname);
                                    } while(prop == null);
                                } while(prop.isIdProperty());
                            } while(prop.isIgnoreProperty());

                            vs1 = r1.getValues(propname);
                            vs2 = r2.getValues(propname);
                        } while(vs1 == null);
                    } while(vs1.isEmpty());
                } while(vs2 == null);
            } while(vs2.isEmpty());

            double high = 0.0D;
            Iterator i$1 = vs1.iterator();

            while(true) {
                String v1;
                do {
                    if(!i$1.hasNext()) {
                        prob = Utils.computeBayes(prob, high);
                        continue label80;
                    }

                    v1 = (String)i$1.next();
                } while(v1.equals(""));

                Iterator i$2 = vs2.iterator();

                while(i$2.hasNext()) {
                    String v2 = (String)i$2.next();
                    if(!v2.equals("")) {
                        try {
                            double e = prop.compare(v1, v2);
                            high = Math.max(high, e);
                        } catch (Exception var18) {
                            throw new DukeException("Comparison of values \'" + v1 + "\' and " + "\'" + v2 + "\' with " + prop.getComparator() + " failed", var18);
                        }
                    }
                }
            }
        }
    }

    public void close() {
        this.database.close();
    }

    private boolean isSameAs(Record r1, Record r2) {
        Iterator i$ = this.config.getIdentityProperties().iterator();

        while(true) {
            Collection vs2;
            Collection vs1;
            do {
                if(!i$.hasNext()) {
                    return false;
                }

                Property idp = (Property)i$.next();
                vs2 = r2.getValues(idp.getName());
                vs1 = r1.getValues(idp.getName());
            } while(vs1 == null);

            Iterator i$1 = vs1.iterator();

            while(i$1.hasNext()) {
                String v1 = (String)i$1.next();
                if(vs2.contains(v1)) {
                    return true;
                }
            }
        }
    }

    private void startProcessing() {
        long start = System.currentTimeMillis();
        Iterator i$ = this.listeners.iterator();

        while(i$.hasNext()) {
            MatchListener listener = (MatchListener)i$.next();
            listener.startProcessing();
        }

        this.callbacks += System.currentTimeMillis() - start;
    }

    private void endProcessing() {
        long start = System.currentTimeMillis();
        Iterator i$ = this.listeners.iterator();

        while(i$.hasNext()) {
            MatchListener listener = (MatchListener)i$.next();
            listener.endProcessing();
        }

        this.callbacks += System.currentTimeMillis() - start;
    }

    private void batchReady(int size) {
        long start = System.currentTimeMillis();
        Iterator i$ = this.listeners.iterator();

        while(i$.hasNext()) {
            MatchListener listener = (MatchListener)i$.next();
            listener.batchReady(size);
        }

        this.callbacks += System.currentTimeMillis() - start;
    }

    private void batchDone() {
        long start = System.currentTimeMillis();
        Iterator i$ = this.listeners.iterator();

        while(i$.hasNext()) {
            MatchListener listener = (MatchListener)i$.next();
            listener.batchDone();
        }

        this.callbacks += System.currentTimeMillis() - start;
    }

    private void registerMatch(Record r1, Record r2, double confidence) {
        long start = System.currentTimeMillis();
        Iterator i$ = this.listeners.iterator();

        while(i$.hasNext()) {
            MatchListener listener = (MatchListener)i$.next();
            listener.matches(r1, r2, confidence);
        }

        this.callbacks += System.currentTimeMillis() - start;
    }

    private void registerMatchPerhaps(Record r1, Record r2, double confidence) {
        long start = System.currentTimeMillis();
        Iterator i$ = this.listeners.iterator();

        while(i$.hasNext()) {
            MatchListener listener = (MatchListener)i$.next();
            listener.matchesPerhaps(r1, r2, confidence);
        }

        this.callbacks += System.currentTimeMillis() - start;
    }

    private void registerNoMatchFor(Record current) {
        long start = System.currentTimeMillis();
        Iterator i$ = this.listeners.iterator();

        while(i$.hasNext()) {
            MatchListener listener = (MatchListener)i$.next();
            listener.noMatchFor(current);
        }

        this.callbacks += System.currentTimeMillis() - start;
    }

    public class Profiler extends AbstractMatchListener {
        private long processing_start;
        private long batch_start;
        private int batch_size;
        private int records;
        private PrintWriter out;

        public Profiler() {
            this.out = new PrintWriter(System.out);
        }

        public void setOutput(Writer outw) {
            this.out = new PrintWriter(outw);
        }

        public void startProcessing() {
            this.processing_start = System.currentTimeMillis();
            System.out.println("Duke version " + Duke.getVersionString());
            System.out.println(Processor.this.getDatabase());
            System.out.println("Threads: " + Processor.this.getThreads());
        }

        public void batchReady(int size) {
            this.batch_start = System.currentTimeMillis();
            this.batch_size = size;
        }

        public void batchDone() {
            this.records += this.batch_size;
            int rs = (int)(1000.0D * (double)this.batch_size / (double)(System.currentTimeMillis() - this.batch_start));
            System.out.println("" + this.records + " processed, " + rs + " records/second; comparisons: " + Processor.this.getComparisonCount());
        }

        public void endProcessing() {
            long end = System.currentTimeMillis();
            double rs = 1000.0D * (double)this.records / (double)(end - this.processing_start);
            System.out.println("Run completed, " + (int)rs + " records/second");
            System.out.println("" + this.records + " records total in " + (end - this.processing_start) / 1000L + " seconds");
            long total = Processor.this.srcread + Processor.this.indexing + Processor.this.searching + Processor.this.comparing + Processor.this.callbacks;
            System.out.println("Reading from source: " + this.seconds(Processor.this.srcread) + " (" + this.percent(Processor.this.srcread, total) + "%)");
            System.out.println("Indexing: " + this.seconds(Processor.this.indexing) + " (" + this.percent(Processor.this.indexing, total) + "%)");
            System.out.println("Searching: " + this.seconds(Processor.this.searching) + " (" + this.percent(Processor.this.searching, total) + "%)");
            System.out.println("Comparing: " + this.seconds(Processor.this.comparing) + " (" + this.percent(Processor.this.comparing, total) + "%)");
            System.out.println("Callbacks: " + this.seconds(Processor.this.callbacks) + " (" + this.percent(Processor.this.callbacks, total) + "%)");
            System.out.println();
            Runtime r = Runtime.getRuntime();
            System.out.println("Total memory: " + r.totalMemory() + ", " + "free memory: " + r.freeMemory() + ", " + "used memory: " + (r.totalMemory() - r.freeMemory()));
        }

        private String seconds(long ms) {
            return "" + (int)(ms / 1000L);
        }

        private String percent(long ms, long total) {
            return "" + (int)((double)(ms * 100L) / (double)total);
        }
    }

    class MatchThread extends Thread {
        private Collection<Record> records;
        private boolean matchall;

        public MatchThread(int threadno, int recordcount, boolean matchall) {
            super("MatchThread " + threadno);
            this.records = new ArrayList(recordcount);
            this.matchall = matchall;
        }

        public void run() {
            Iterator i$ = this.records.iterator();

            while(i$.hasNext()) {
                Record record = (Record)i$.next();
                Processor.this.match(record, this.matchall);
            }

        }

        public void addRecord(Record record) {
            this.records.add(record);
        }
    }

    static class PropertyComparator implements Comparator<Property> {
        PropertyComparator() {
        }

        public int compare(Property p1, Property p2) {
            double diff = p1.getLowProbability() - p2.getLowProbability();
            return diff < 0.0D?-1:(diff > 0.0D?1:0);
        }
    }
}
