package no.priv.garshol.duke;

import no.priv.garshol.duke.Logger;
import no.priv.garshol.duke.RecordIterator;

public interface DataSource {
    RecordIterator getRecords(DataSource source);

    void setLogger(Logger var1);
}
