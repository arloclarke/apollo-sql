package no.priv.garshol.duke.datasources;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Properties;
import java.text.DecimalFormat;


import mimir.ProgressTracker;
import no.priv.garshol.duke.Record;
import no.priv.garshol.duke.RecordIterator;
import no.priv.garshol.duke.comparators.ExactComparator;
import no.priv.garshol.duke.utils.JDBCUtils;

import java.util.ArrayList;
import java.sql.ResultSetMetaData;
import no.priv.garshol.duke.DataSource;

public class JDBCDataSource extends ColumnarDataSource {
    private String jdbcuri;
    private String driverclass;
    private String username;
    private String password;
    private String query;

    public JDBCDataSource() {
    }

    public void setConnectionString(String str) {
        this.jdbcuri = str;
    }

    public void setDriverClass(String klass) {
        this.driverclass = klass;
    }

    public void setUserName(String username) {
        this.username = username;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public void setQuery(String query) {
        this.query = query;
    }

    public String getQuery() {
        return this.query;
    }

    public String getDriverClass() {
        return this.driverclass;
    }

    public String getConnectionString() {
        return this.jdbcuri;
    }

    public String getUserName() {
        return this.username;
    }

    public String getPassword() {
        return this.password;
    }

    public RecordIterator getRecords(DataSource source) {

        this.verifyProperty(this.jdbcuri, "connection-string");
        this.verifyProperty(this.driverclass, "driver-class");
        this.verifyProperty(this.query, "query");

        int total = 0;
        int limit_size = 50;
        Properties e = null;
        int offset = 0;
        ArrayList results = new ArrayList<ArrayList<String>>();
        ArrayList cols = new ArrayList<String>();

        // setup
        try {
            e = new Properties();
            if (this.username != null) {
                e.put("user", this.username);
            }

            if (this.password != null) {
                e.put("password", this.password);
            }
            Statement stmt = JDBCUtils.open(this.driverclass, this.jdbcuri, e);
            System.out.println("[DEBUG] Preforming Setup Query");
            ResultSet rs = stmt.executeQuery("select count(*) from (" + this.query + ") as temp_count");
            System.out.println("[DEBUG] Completed Setup Query");
            rs.next();
            total = rs.getInt(1);
            ProgressTracker.setTotal(total);
            JDBCUtils.close(stmt);
        } catch (Exception e1) {
            System.out.println(e1.getMessage());
        }



//        DecimalFormat twoDForm = new DecimalFormat("#.##");
        int counter = 0;
        while (counter < 2) {

            if (offset > 0) {
                System.out.println("[DEBUG] Preforming SQL query for remainder of database...");
            } else {
                System.out.println("[DEBUG] Preforming initial SQL query...");
            }

//            double percentage = Double.valueOf(twoDForm.format((offset/(double)total) * 100));
//            System.out.print("[INFO] Query is " + Double.toString(percentage) + "% complete\r");
            String selector = "LIMIT " + limit_size + " OFFSET ";
            if (offset > 0) {

                selector = "OFFSET ";

            }

            try {
                Statement stmt = JDBCUtils.open(this.driverclass, this.jdbcuri, e);
                ResultSet rs = stmt.executeQuery(this.query + " " + selector + offset);
                int row_length = rs.getMetaData().getColumnCount();

                if (offset == 0) {
                    ResultSetMetaData rsmd = rs.getMetaData();
                    cols.add(rsmd.getColumnName(1));
                    cols.add(rsmd.getColumnName(2));
                    cols.add(rsmd.getColumnName(3));
                    cols.add(rsmd.getColumnName(4));
                    cols.add(rsmd.getColumnName(5));
                }
                System.out.println("[INFO] Query completed, generating result set");
                while (rs.next()) {
                    ArrayList row = new ArrayList<>();
                    for (int i = 1; i <= row_length; i++) {
                        row.add(rs.getString(i));
                    }
                    results.add(row);
                }
                JDBCUtils.close(stmt);

                // Attempt garbage collection of SQL result set
                rs = null;
                System.gc();

                offset += limit_size;
                counter++;

            } catch (SQLException var4) {
                throw new RuntimeException(var4);
            }
        }

//        for (int i = 0; i < results.size(); i++) {
//            ArrayList row = (ArrayList)results.get(i);
//            System.out.println(row.get(0) + ", " + row.get(1));
//        }

        try {
            System.out.println("[INFO] SQL query complete, starting deduplication...");
            return new JDBCDataSource.JDBCIterator(results, cols, source);
        } catch (SQLException var6) {
            throw new RuntimeException(var6);
        }
    }

    protected String getSourceName() {
        return "JDBC";
    }

    public class JDBCIterator extends RecordIterator {
        private ArrayList records;
        private RecordBuilder builder;
        private ArrayList col_names;

        public JDBCIterator(ArrayList results, ArrayList cols, DataSource source) throws SQLException {

            this.records = results;
            this.col_names = cols;
            builder = new RecordBuilder((ColumnarDataSource)source);
        }

        public boolean hasNext() {
            return !records.isEmpty();
        }

        public Record next() {
            try {
                this.builder.newRecord();
                ArrayList row = (ArrayList)records.get(0);

                if (hasNext())
                    records.remove(0);

                for (int i = 0; i < row.size(); i++) {
                    String value = (String)row.get(i);
                    String name = (String)col_names.get(i);
                    this.builder.addValue(name, value);
                }

                return this.builder.getRecord();
            } catch (Exception var4) {
                throw new RuntimeException(var4);
            }
        }

        public void close() {}
    }
}
