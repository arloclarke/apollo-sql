package mimir;

import no.priv.garshol.duke.*;
import no.priv.garshol.duke.matchers.PrintMatchListener;
import no.priv.garshol.duke.matchers.ClassDatabaseMatchListener;
import java.io.FileInputStream;
import java.util.Properties;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.Transformer;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.stream.StreamResult;
import org.w3c.dom.Document;
import java.io.File;
import java.util.logging.Logger;
import java.util.logging.Level;

public class Deduplicate {

    private static Logger logger = Logger.getLogger(ApolloLogger.class.getName());
    private static boolean debug = false;

    // This method will replace the postges sting in the duke config files with the value in config.properties
    private static void set_postgres_in_config(String filepath) throws java.io.IOException {

        Properties prop = new Properties();
        prop.load(new FileInputStream("config.properties"));
        String postgres_string = prop.getProperty("POSTGRES_URL");
        String user = prop.getProperty("USERNAME");
        String password = prop.getProperty("PASSWORD");

        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        factory.setValidating(false);
        factory.setIgnoringElementContentWhitespace(true);
        try {
            DocumentBuilder builder = factory.newDocumentBuilder();
            File file = new File(filepath);
            Document doc = builder.parse(file);

            // set connection sting
            doc.getDocumentElement().getElementsByTagName("param")
                    .item(1).getAttributes().getNamedItem("value").setNodeValue(postgres_string);

            // set username
            doc.getDocumentElement().getElementsByTagName("param")
                    .item(2).getAttributes().getNamedItem("value").setNodeValue(user);

            // set password
            doc.getDocumentElement().getElementsByTagName("param")
                    .item(3).getAttributes().getNamedItem("value").setNodeValue(password);

            TransformerFactory transformerFactory = TransformerFactory.newInstance();
            Transformer transformer = transformerFactory.newTransformer();

            //for pretty print
            transformer.setOutputProperty(OutputKeys.INDENT, "yes");
            DOMSource source = new DOMSource(doc);

            StreamResult file_out = new StreamResult(new File(filepath));

            //write data
            transformer.transform(source, file_out);

        } catch (Exception e) {

            logger.log(Level.SEVERE, e.getMessage());
        }

    }

    public static void main(String[] argv) throws Exception {

        System.out.println("[INFO] Preforming Duke Setup");
        set_postgres_in_config(argv[0]);

        // Ask for memory resources from dead objects
        System.gc();

        mimir.KeywordCleaner.buildBlacklist();

        ProgressTracker.resetCount();

        Configuration config = ConfigLoader.load(argv[0]);

        Processor proc = new Processor(config);

        proc.setPerformanceProfiling(true);

        PythonInterface knowledgebase_interface = new PythonInterface(argv[1]);

        if (debug)
            proc.addMatchListener(new PrintMatchListener(true, true, true, false, config.getProperties(), true));
        else
            proc.addMatchListener(new ClassDatabaseMatchListener(config, knowledgebase_interface));

        System.out.println("[INFO] Starting deduplication...");
        proc.deduplicate();
        proc.close();
        System.out.println("[INFO] Finished deduplication, pushing to RabbitMQ");

        knowledgebase_interface.send_to_python();

    }
}
