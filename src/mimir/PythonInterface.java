package mimir;

import no.priv.garshol.duke.EquivalenceClassDatabase;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import com.rabbitmq.client.ConnectionFactory;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.Channel;
import java.util.Properties;
import java.io.FileInputStream;
import java.util.concurrent.SynchronousQueue;
import java.util.logging.Logger;
import java.util.logging.Level;
import java.io.FileWriter;
import java.io.BufferedWriter;
import java.io.PrintWriter;


public class PythonInterface implements EquivalenceClassDatabase {

    private static Logger logger = Logger.getLogger(ApolloLogger.class.getName());
    private ArrayList<String[]> linked_people;
    private final static String QUEUE_NAME = "java_to_python";
    private ConnectionFactory factory;
    private Connection connection;
    private Channel channel;
    private String mimir_table;
    private String rabbitmq_url;

    public PythonInterface(String table) throws java.io.IOException {

        linked_people = new ArrayList<>();
        //factory = new ConnectionFactory();
        mimir_table = table;

        Properties prop = new Properties();
        prop.load(new FileInputStream("config.properties"));
        rabbitmq_url = prop.getProperty("RABBITMQ_BROKER");
    }

    public ArrayList<String[]> getLinked() {

        return linked_people;
    }

    public void send_to_python() throws java.io.IOException, java.util.concurrent.TimeoutException {
        //connect();
        sendData();
        //disconnect();
    }

    // Connect to RabbitMQ broker
    private void connect() throws java.io.IOException {

        try {

            factory.setHost(rabbitmq_url);
            connection = factory.newConnection();
            channel = connection.createChannel();

        } catch (java.util.concurrent.TimeoutException E) {
            logger.log(Level.SEVERE, "Failed to connect to RabbitMQ: "+E.getMessage());
        }
    }

    // Disconnect from RabbitMQ broker
    private void disconnect() throws java.util.concurrent.TimeoutException, java.io.IOException
    {
        channel.close();
        connection.close();
    }

    // Send data to the RabbitMQ broker
    private void sendData() throws java.io.IOException {

// Below commented out code is for the rabbitmq / distributed cluster version
//
//        channel.queueDeclare(QUEUE_NAME, false, false, false, null);
//
//        channel.basicPublish("", QUEUE_NAME, null, ("START,"+mimir_table).getBytes());
//        logger.log(Level.INFO, "Start: " + mimir_table);
//
//        for (int i = 0; i < linked_people.size(); i++) {
//
//            String message = linked_people.get(i)[0] + ", " + linked_people.get(i)[1];
//            channel.basicPublish("", QUEUE_NAME, null, message.getBytes());
//
//            logger.log(Level.INFO, "Sent '" + message + "'");
//        }
//
//        channel.basicPublish("", QUEUE_NAME, null, ("DONE,"+mimir_table).getBytes());
//        logger.log(Level.INFO, "End: " + mimir_table);


        // for same machine duke + python
        System.out.println("[DEBUG] Writing Apollo output to disk...");
        try(FileWriter fw = new FileWriter("duke_output-unlimited_comp.txt", true);
            BufferedWriter bw = new BufferedWriter(fw);
            PrintWriter out = new PrintWriter(bw))
        {
            out.println("START," + mimir_table);
            logger.log(Level.INFO, "Start: " + mimir_table);


            for (int i = 0; i < linked_people.size(); i++) {

                String message = linked_people.get(i)[0] + ", " + linked_people.get(i)[1];
                out.println(message);
                logger.log(Level.INFO, "Sent '" + message + "'");
            }

            out.println("DONE," + mimir_table);


        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    // Add a link between two id's
    public void addLink(String id1, String id2) {

        String[] ids = {id1, id2};

        linked_people.add(ids);

    }

    // Prints linked id's (commit() is necessary for EquivalenceClassDatabase interface)
    public void commit() {

//        for (int i = 0; i < linked_people.size(); i++)
//            logger.log(Level.INFO, "linked: " + linked_people.get(i)[0] + ", "+linked_people.get(i)[1]);
    }

    /*

    The below methods are only implemented to satisfy the EquivalenceClassDatabase interface

     */
    public Collection<String> getClass(String id) {

        return new Collection<String>() {
            @Override
            public int size() {
                return 0;
            }

            @Override
            public boolean isEmpty() {
                return false;
            }

            @Override
            public boolean contains(Object o) {
                return false;
            }

            @Override
            public Iterator<String> iterator() {
                return null;
            }

            @Override
            public Object[] toArray() {
                return new Object[0];
            }

            @Override
            public <T> T[] toArray(T[] a) {
                return null;
            }

            @Override
            public boolean add(String s) {
                return false;
            }

            @Override
            public boolean remove(Object o) {
                return false;
            }

            @Override
            public boolean containsAll(Collection<?> c) {
                return false;
            }

            @Override
            public boolean addAll(Collection<? extends String> c) {
                return false;
            }

            @Override
            public boolean removeAll(Collection<?> c) {
                return false;
            }

            @Override
            public boolean retainAll(Collection<?> c) {
                return false;
            }

            @Override
            public void clear() {

            }
        };
    }

    public int getClassCount() {

        return 0;
    }

    public Iterator<Collection<String>> getClasses() {

        return new Iterator<Collection<String>>() {
            @Override
            public boolean hasNext() {
                return false;
            }

            @Override
            public Collection<String> next() {
                return null;
            }
        };
    }

}