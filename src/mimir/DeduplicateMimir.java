package mimir;

import java.io.FileInputStream;
import java.io.SyncFailedException;
import java.util.Properties;
import java.util.logging.Logger;
import java.util.logging.Level;
import com.rabbitmq.client.*;

public class DeduplicateMimir {

    private static final String QUEUE_NAME = "python_to_java";

    public static void main(String[] argv) throws Exception {

        ApolloLogger.main(new String[] {});
        Logger logger = Logger.getLogger(ApolloLogger.class.getName());

//        Properties prop = new Properties();
//        prop.load(new FileInputStream("config.properties"));
//        String rabbitmq_url = prop.getProperty("RABBITMQ_BROKER");
//
//
//        Connection connection = null;
//        Channel channel = null;
//        try {
//            ConnectionFactory factory = new ConnectionFactory();
//            factory.setHost(rabbitmq_url);
//            connection = factory.newConnection();
//            channel = connection.createChannel();
//            channel.queueDeclare(QUEUE_NAME, false, false, false, null);
//        } catch (Exception e) {
//            logger.log(Level.SEVERE, e.getMessage());
//        }

        // Commented Out because only non-authors should run
        // logger.log(Level.INFO, "Deduplicating People (Publications)");
        Deduplicate.main(new String[] {"config/person_article_config.xml", "Person_Article"});



        logger.log(Level.INFO, "Deduplicating People (CT, Grants)");

//        logger.log(Level.INFO, "Deduplicating Keywords...");
//        Deduplicate.main(new String[] {"config/keyword_config.xml", "Keyword"});
//        logger.log(Level.INFO, "Deduplicating Mesh Terms...");
//        Deduplicate.main(new String[] {"config/mesh_config.xml", "MeshTerm"});
//        logger.log(Level.INFO, "Deduplicating Countries...");
//        Deduplicate.main(new String[] {"config/country_config.xml", "Country"});
//        logger.log(Level.INFO, "Deduplicating Journals...");
//        Deduplicate.main(new String[] {"config/journal_config.xml", "Journal"});
//        logger.log(Level.INFO, "Deduplicating Articles...");
//        Deduplicate.main(new String[] {"config/pubmed_config.xml", "Article"});
//        logger.log(Level.INFO, "Deduplicating Organizations...");
//        Deduplicate.main(new String[] {"config/org_config.xml", "Organization"});
//        logger.log(Level.INFO, "Deduplicating Grant Projects...");
//        Deduplicate.main(new String[] {"config/grant_project_config.xml", "Grant_Project"});
//        logger.log(Level.INFO, "Deduplicating Grant Entries...");
//        Deduplicate.main(new String[] {"config/grant_entry_config.xml", "Grant_Entry"});
//        logger.log(Level.INFO, "Deduplicating Clinical Trials...");
//        Deduplicate.main(new String[] {"config/ct_config.xml", "Clinical_Trial"});
//        logger.log(Level.INFO, "Deduplicating CT Collaborators...");
//        Deduplicate.main(new String[] {"config/collab_config.xml", "CollaboratorOrg"});

        logger.log(Level.INFO, "Deduplicating People...");
        Deduplicate.main(new String[] {"config/person_config.xml", "Person"});

//        channel.close();
//        connection.close();

    }
}
