//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package mimir;

import no.priv.garshol.duke.Comparator;

public class AssociatedOrgComparator implements Comparator {
    public AssociatedOrgComparator() {
    }

    public double compare(String s1, String s2) {

        // No orgs, 50/50 if they are related
        if (isNull(s1) || isNull(s2))
            return 0.5;

        s1 = s1.toLowerCase();
        s2 = s2.toLowerCase();

        int len = Math.min(s1.length(), s2.length());
        int maxlen = Math.max(s1.length(), s2.length());
        if((double)len / (double)maxlen <= 0.5D) {
            return 0.0D;
        } else if(len == maxlen && s1.equals(s2)) {
            return 1.0D;
        } else {
            int dist = Math.min(compactDistance(s1, s2), len);
            return 1.0D - (double)dist / (double)len;
        }
    }

    public boolean isTokenized() {
        return true;
    }


    private  boolean isNull(String org)
    {
        if (org == null || org.isEmpty() || org.trim().isEmpty())
            return true;
        else
            return false;

    }

    public static int distance(String s1, String s2) {
        if(s1.length() == 0) {
            return s2.length();
        } else if(s2.length() == 0) {
            return s1.length();
        } else {
            int s1len = s1.length();
            int[] matrix = new int[(s1len + 1) * (s2.length() + 1)];

            int ix1;
            for(ix1 = 0; ix1 <= s2.length(); matrix[ix1 * s1len] = ix1++) {
                ;
            }

            for(ix1 = 0; ix1 <= s1len; matrix[ix1] = ix1++) {
                ;
            }

            for(ix1 = 0; ix1 < s1len; ++ix1) {
                char ch1 = s1.charAt(ix1);

                for(int ix2 = 0; ix2 < s2.length(); ++ix2) {
                    byte cost;
                    if(ch1 == s2.charAt(ix2)) {
                        cost = 0;
                    } else {
                        cost = 1;
                    }

                    int left = matrix[ix1 + (ix2 + 1) * s1len] + 1;
                    int above = matrix[ix1 + 1 + ix2 * s1len] + 1;
                    int aboveleft = matrix[ix1 + ix2 * s1len] + cost;
                    matrix[ix1 + 1 + (ix2 + 1) * s1len] = Math.min(left, Math.min(above, aboveleft));
                }
            }

            return matrix[s1len + s2.length() * s1len];
        }
    }

    public static int compactDistance(String s1, String s2) {
        if(s1.length() == 0) {
            return s2.length();
        } else if(s2.length() == 0) {
            return s1.length();
        } else {
            int maxdist = Math.min(s1.length(), s2.length()) / 2;
            int s1len = s1.length();
            int[] column = new int[s1len + 1];
            byte ix2 = 0;
            char ch2 = s2.charAt(ix2);
            column[0] = 1;

            int above;
            int smallest;
            for(above = 1; above <= s1len; ++above) {
                smallest = s1.charAt(above - 1) == ch2?0:1;
                column[above] = Math.min(column[above - 1], above - 1) + smallest;
            }

            above = 0;

            for(int var12 = 1; var12 < s2.length(); ++var12) {
                ch2 = s2.charAt(var12);
                above = var12 + 1;
                smallest = s1len * 2;

                for(int ix1 = 1; ix1 <= s1len; ++ix1) {
                    int cost = s1.charAt(ix1 - 1) == ch2?0:1;
                    int value = Math.min(Math.min(above, column[ix1 - 1]), column[ix1]) + cost;
                    column[ix1 - 1] = above;
                    above = value;
                    smallest = Math.min(smallest, value);
                }

                column[s1len] = above;
                if(smallest > maxdist) {
                    return smallest;
                }
            }

            return above;
        }
    }
}
