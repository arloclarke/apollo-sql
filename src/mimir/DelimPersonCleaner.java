package mimir;

import no.priv.garshol.duke.Cleaner;
import java.util.ArrayList;


// There must be a better way to do this...
public class DelimPersonCleaner implements Cleaner {


    // Remove any duplicate pids
    public String clean(String var1) {

        String[] tokenized = var1.split("!");
        ArrayList<String> check = new ArrayList<>();

        int len = tokenized.length;

        if (len == 1) {
            return var1;
        }

        if (len == 2) {
            if (tokenized[0].equals(tokenized[1]))
                return tokenized[0];
            else
                return var1;
        }

        boolean found = false;
        for (int i = 0; i < len - 1; i++) {

            for (int j = 0; j < check.size() - 1; j++) {

                if (check.get(j).equals(tokenized[i]))
                    found = true;
            }

            if (!found || check.size() == 0) {
                check.add(tokenized[i]);
            } else {
                found = false;
            }
        }

        String to_return = "";

        for (int i = 0; i < check.size() - 1; i++) {

            if (i == check.size() - 2)
                to_return += check.get(i);
            else
                to_return += (check.get(i) + '!');
        }

        return to_return;
    }
}
