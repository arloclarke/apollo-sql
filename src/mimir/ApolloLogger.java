package mimir;

import no.priv.garshol.duke.datasources.JDBCDataSource;

import java.io.IOException;
import java.util.logging.FileHandler;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;

public class ApolloLogger {

    //TODO: gitignore log file as well as config.properties
    public static void main(String[] args) {

        Logger logger = Logger.getLogger(ApolloLogger.class.getName());
        FileHandler fh;

        try {

            fh = new FileHandler("ApolloLogger.log", true);
            logger.addHandler(fh);
            logger.setUseParentHandlers(false);
            logger.setLevel(Level.ALL);
            SimpleFormatter formatter = new SimpleFormatter();
            fh.setFormatter(formatter);

            logger.info("Initialize Logger");

        } catch (SecurityException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}