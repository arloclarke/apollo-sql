package mimir;

import no.priv.garshol.duke.Comparator;
import no.priv.garshol.duke.matchers.ClassDatabaseMatchListener;

// compares the pid's of person records associated with the person records being compared
public class KeywordComparator implements Comparator {

    public double compare(String v1, String v2) {

        // The SQL query deliminates values with "!" because the char will not be used in sql fields
        String[] tokenized1 = v1.split("!");
        String[] tokenized2 = v2.split("!");

        int len1 = tokenized1.length, len2 = tokenized2.length;

        double possible_associated = Math.min(len1, len2), associated_count = 0.0;

        for (int i = 0; i < len1 - 1; i++) {

                for (int j = 0; j < len2 - 1; j++) {


                    if (!isNull(tokenized1[i]) && !isNull(tokenized1[i])) {


                        if (fuzzy_match(tokenized1[i], tokenized2[j]) >= 0.8) associated_count += 1.0;
                    }   else {
                        possible_associated -= 1;
                    }
                }

        }

        if (possible_associated <= 0.0) return 0.5; // no information learned
        else
            return associated_count / possible_associated;
    }

    // Tells Duke that the comparator breaks up values into tokens (impacts indexing of values)
    public boolean isTokenized() {
        return true;
    }


    public static int compactDistance(String s1, String s2) {
        if (s1.length() == 0) {
            return s2.length();
        } else if (s2.length() == 0) {
            return s1.length();
        } else {
            int maxdist = Math.min(s1.length(), s2.length()) / 2;
            int s1len = s1.length();
            int[] column = new int[s1len + 1];
            byte ix2 = 0;
            char ch2 = s2.charAt(ix2);
            column[0] = 1;

            int above;
            int smallest;
            for (above = 1; above <= s1len; ++above) {
                smallest = s1.charAt(above - 1) == ch2 ? 0 : 1;
                column[above] = Math.min(column[above - 1], above - 1) + smallest;
            }

            above = 0;

            for (int var12 = 1; var12 < s2.length(); ++var12) {
                ch2 = s2.charAt(var12);
                above = var12 + 1;
                smallest = s1len * 2;

                for (int ix1 = 1; ix1 <= s1len; ++ix1) {
                    int cost = s1.charAt(ix1 - 1) == ch2 ? 0 : 1;
                    int value = Math.min(Math.min(above, column[ix1 - 1]), column[ix1]) + cost;
                    column[ix1 - 1] = above;
                    above = value;
                    smallest = Math.min(smallest, value);
                }

                column[s1len] = above;
                if (smallest > maxdist) {
                    return smallest;
                }
            }

            return above;
        }
    }

    public double fuzzy_match(String s1, String s2) {
        int len = Math.min(s1.length(), s2.length());
        int maxlen = Math.max(s1.length(), s2.length());
        if((double)len / (double)maxlen <= 0.5D) {
            return 0.0D;
        } else if(len == maxlen && s1.equals(s2)) {
            return 1.0D;
        } else {
            int dist = Math.min(compactDistance(s1, s2), len);
            return 1.0D - (double)dist / (double)len;
        }
    }

    private  boolean isNull(String org)
    {
        if (org == null || org.isEmpty() || org.trim().isEmpty())
            return true;
        else
            return false;

    }
}