package mimir;

/**
 * Created by arloclarke on 3/28/16.
 */


public class ProgressTracker {

    static long ideal_total = 0;
    static long count = 0;

    public ProgressTracker() throws java.io.IOException {}

    public static void setTotal(long total)  {
        ideal_total = total;
    }

    public static void addCount() {
        count++;
    }

    public static void resetCount() {
        count = 0;
    }

    public static void getPercent() {
        System.out.print("[INFO] Deduplication is at " + count * 100 / ideal_total + "%\r");
        if (count * 100 / ideal_total == 100) {
            System.out.println("[INFO] Deduplication complete, processing results");
        }
    }
}
