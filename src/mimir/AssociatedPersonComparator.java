package mimir;

import no.priv.garshol.duke.Comparator;

// compares the pid's of person records associated with the person records being compared
public class AssociatedPersonComparator implements Comparator {

    public double compare(String v1, String v2) {


        // The SQL query deliminates values with "!" because the char will not be used in sql fields
        String[] tokenized1 = v1.split("!");
        String[] tokenized2 = v2.split("!");

        int len1 = tokenized1.length, len2 = tokenized2.length;

        // Both people are only associated with themselves
        if (len1 == 1 && len2 == 1) {
            return 0.5;
        }

        double possible_associated = Math.min(len1, len2), associated_count = 0.0;

        for (int i = 0; i < len1 - 1; i++) {

            for (int j = 0; j < len2 - 1; j++) {

                if (tokenized1[i].equals(tokenized2[j])) associated_count += 1.0;
            }
        }

        // prevent divide by zero
        if (possible_associated == 0.0) return 0.0;
        else
            return associated_count / possible_associated;
    }

    // Tells Duke that the comparator breaks up values into tokens (impacts indexing of values)
    public boolean isTokenized() {
        return true;
    }

}