package mimir;

import no.priv.garshol.duke.Cleaner;
import java.util.HashSet;
import java.io.FileInputStream;
import java.io.DataInputStream;
import java.io.BufferedReader;
import java.io.InputStreamReader;

public class KeywordCleaner implements Cleaner {

    private static HashSet<String> blacklist;

    // Remove any duplicate pids
    public String clean(String var1) {

        HashSet<String> checked = new HashSet<>();
        String[] tokenized = var1.split("!");

        int kwd_len = tokenized.length;
        String to_return = "";

        for (int i = 0; i < kwd_len - 1; i++) {

            if (!isNull(tokenized[i])) {
                if (!blacklist.contains(tokenized[i]) && !checked.contains(tokenized[i])) {
                    to_return += (tokenized[i] + "!");
                }
                checked.add(tokenized[i]);
            }
        }

        return to_return;
    }

    public static void buildBlacklist() throws java.io.IOException {

        blacklist = new HashSet<>();

        FileInputStream fstream = new FileInputStream("keyword_blacklist.csv");
        // Get the object of DataInputStream
        DataInputStream in = new DataInputStream(fstream);
        BufferedReader br = new BufferedReader(new InputStreamReader(in));
        String strLine;
        //Read File Line By Line
        while ((strLine = br.readLine()) != null)   {
            blacklist.add(strLine);
        }
        in.close();
    }

    private  boolean isNull(String org)
    {
        if (org == null || org.isEmpty() || org.trim().isEmpty())
            return true;
        else
            return false;

    }
}
