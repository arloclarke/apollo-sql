(c) 2016 Mimir Consulting LLC

Apollo: Greek god of knowledge, light, and prophecy (Also the moon landing program).

This maven project is the version of Apollo that is to be used with the SQL database
before the implementation of Titan Graph.

This project is most compatible with the "IntelliJ IDEA" IDE

To Compile (skipping tests): 
    
    in root dir, $ mvn clean install -DskipTests

To Run:

    RabbitMQ must be running on localhost

    in root dir, $ mvn exec:java -Dexec.mainClass=mimir.DeduplicateMimir


NOTE:

    - If two records have the same ID, Duke does not return the records as matching because a record will not return as
      matching to itself.
      
    - In the config.xml file, Low probability = probability when values don't match. High = probability when they do
      match.


TESTING:
    
    test classes in root/src/test/java
    (this dir is named so that test jar dependancies can be found)

    run all unit tests:   $ mvn test
    run single test:      $ mvn test -Dtest=FooBar



Start manually:

pg_ctl -D /usr/local/var/postgres -l /usr/local/var/postgres/server.log start

Stop manually:

pg_ctl -D /usr/local/var/postgres stop -s -m fast


delete everything in elasticsearch:
curl -XDELETE 'http://localhost:9200/*'
